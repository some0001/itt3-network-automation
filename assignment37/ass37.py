from netmiko import ConnectHandler
from threading import Thread

class Ass37():
    '''
    Class made to fulfill requirements of network automation assignment 37

    The class is used for netmiko connection to junos devices whose parameter
    must best provided in the class init arguments. Once connected, object of
    this class is able to retrieve and push configurations to the junos device
    '''

    def __init__(self, *args, **kwargs):
        self.connEstablished = False
        print('Establishing SSH connection to {}@{}...'.format(kwargs['username'], kwargs['host']))
        try:
            self.netConn = ConnectHandler(**kwargs)
            self.connEstablished = True
            print('SSH connection to {}@{} established'.format(kwargs['username'], kwargs['host']))
        except:
            print('SSH connection to {}@{} could not be established'.format(kwargs['username'], kwargs['host']))
            self.connEstablished = False


    def feedback_print(func):
        def wrapper(self, *args, **kwargs):
            print('Processing command ({}) over ssh to the junos device (hostname: {})...'.format(func.func_name, self.netConn.host))
            func(self, *args, **kwargs)
        return wrapper

    def __commit(self):
        self.netConn.commit()

    def __cfg_mode(self, cmd = None, filename = None, commit = False):
        self.netConn.config_mode()
        cmd_outp = None

        if filename:
            self.netConn.send_config_from_file(filename)
        elif cmd:
            cmd_outp = self.netConn.send_command(cmd)

        if commit is True:
            self.__commit()

        self.netConn.exit_config_mode()

        return cmd_outp
    
    @feedback_print
    def execute_cmds_from_file(self, filename, commitFlag = True):
        '''
        Executes commands from a specified file on a junos device that 
        the class object is connected to.
        '''

        self.__cfg_mode(filename = filename, commit = commitFlag)

    @feedback_print
    def save_running_conf_to_file(self, filename):
        '''
        Saves current running configuration to a file from a junos device 
        that the class object is connected to
        '''

        runningConf = self.__cfg_mode(cmd = 'show')
        with open(filename, 'w') as f:
            f.write(runningConf)
       
def threaded_func_call(func):
    def wrapper(*args, **kwargs):
        th = Thread(target = func, args = args, kwargs = kwargs)
        return th
    return wrapper

@threaded_func_call
def NC_procedure(*args, **kwargs):
    '''
    Performs procedure that
    the assignment requires
    '''
    NC = Ass37(**kwargs)

    if NC.connEstablished is False:
        return

    NC.save_running_conf_to_file('beforeConf.txt')
    NC.execute_cmds_from_file('setConf.txt')
    NC.save_running_conf_to_file('afterConf.txt')

vSRX_count = 2

vSRX_devices = list()

for i in range(1, vSRX_count+1):
    vSRX_devices.append({
    'device_type': 'juniper_junos',
    'host':   '192.168.10.{}'.format(100+i),
    'username': 'root',
    'password': 'Juniper',
    'port' : '22'
    })

def main():
    NC_threads = list()

    for deviceParam in vSRX_devices:
        thread = NC_procedure(**deviceParam)
        thread.start()
        NC_threads.append(thread)

    for thread in NC_threads:
        thread.join()

main()
